// more info:
// https://cli.vuejs.org/config

module.exports = {
    publicPath: process.env.NODE_ENV == 'production'? '.' : '/',
    outputDir: process.env.NODE_ENV == 'production'? 'output' : 'dist',
    assetsDir: process.env.NODE_ENV == 'production'? '.' : '',
    crossorigin: process.env.NODE_ENV == 'production'? undefined : undefined,
    productionSourceMap: process.env.NODE_ENV == 'production'? false : true,
    configureWebpack: {
        devtool: 'source-map',
    },
    devServer: {
        // proxy: 'http://127.0.0.1:60000',
    }
};
