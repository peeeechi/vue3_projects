import Vue from 'vue';
import VM from './viewmodel.vue';

new Vue({
    components: {
        VM,
    },
    render: (h) => h(VM),
}).$mount('#app');



