module.exports = {
    publicPath: process.env.NODE_ENV === 'production' ? "." : "/",
    outputDir: process.env.NODE_ENV === 'production' ? 'output' : 'dist',
    assetsDir: process.env.NODE_ENV === 'production' ? 'static' : '',
    pages: {
        page1: {
            entry: './src/page1/index.ts', // エントリーポイントとなるjs, ts
            template: './public/index.html', // テンプレートのHTML
            filename: 'page1.html' // build時に出力されるファイル名
        },
        page2: {
            entry: './src/page2/index.ts', // エントリーポイントとなるjs, ts
            template: './public/index.html', // テンプレートのHTML
            filename: 'page2.html' // build時に出力されるファイル名
        },
    
    }
};
