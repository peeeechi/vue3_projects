import * as three from 'three';
import { Scene } from 'three';

export default class ThreeModelRenderer {

    private materialTable: {[key: string]: () => three.Material } = {
        MeshBasicMaterial: () => new three.MeshBasicMaterial({color: 0x33ee77}),
        MeshDepthMaterial: () => new three.MeshDepthMaterial({colorWrite: true}),
        MeshNormalMaterial: () => new three.MeshNormalMaterial({colorWrite: true}),
        MeshLambertMaterial: () => new three.MeshLambertMaterial({color: 0x33ee77, clipShadows: true}),
        MeshPhongMaterial: () => new three.MeshPhongMaterial({color: 0x33ee77, clipShadows: true}),
        MeshStandardMaterial: () => new three.MeshStandardMaterial({color: 0x33ee77, roughness: 0.2}),
    };

    private camera: three.PerspectiveCamera = new three.PerspectiveCamera();
    private scene: three.Scene = new three.Scene();
    private box: three.Mesh = new three.Mesh();
    private renderer: three.WebGLRenderer = new three.WebGLRenderer();

    private target: three.Mesh = new three.Mesh();

    private step: number = 0;

    private renderingID: number = -1;
    public get Box(): three.Mesh {
        return this.box;
    }

    private renderAreaName: string;

    constructor(renderAreaName: string) {
        this.renderAreaName = renderAreaName;
    }

    public Init() {
        // Scene 作成

        this.scene = new three.Scene();

        // Cameraの追加
        this.camera = new three.PerspectiveCamera(
            90,
            window.innerWidth / window.innerHeight,
            0.1,
            1000);

        const axes = new three.AxesHelper(20);
        this.scene.add(axes);

        // this.scene.add(this.camera);

        const planeGeo = new three.PlaneGeometry(60, 60);
        const planeMat = new three.MeshLambertMaterial({
            color: 0xeeeeee,
        });

        const plane = new three.Mesh(planeGeo, planeMat);
        plane.receiveShadow = true;
        plane.position.set(0, -2, 0);
        plane.rotation.x = -0.5 * Math.PI;

        this.scene.add(plane);


        // オブジェクトの追加
        // const boxGeo = new three.BoxGeometry(
        //     2,          // width
        //     2,          // height
        //     2,          // depth
        //     undefined,  // widthSegments
        //     undefined,  // heightSegments
        //     undefined,  // depthSegments
        // );
        const boxGeo = new three.TorusGeometry(
           2,
           0.2,
           10,
           100,
        );

        const material = new three.MeshLambertMaterial(
            {
                color: 0x00eeee,
            },
        );

        this.camera.position.set(5, 5, 5);

        this.box = new three.Mesh(boxGeo, material);
        this.box.castShadow = true;


        this.box.position.x = 0;
        this.box.position.y = 0;
        this.box.position.z = 0;
        this.scene.add(this.box);

        this.target = this.Box;

        // 光源の追加

        const direc = new three.AmbientLight(0xffffff, 0.4);

        direc.position.set(0, 10, 0);
        this.scene.add(direc);
        const light = new three.PointLight(
            0x888888,
            4,
        );

        light.position.set(4, 2, 4);
        light.castShadow = true;

        this.scene.add(light);



        // レンダラーの追加

        this.renderer = new three.WebGLRenderer({
            clearColor: 0x000000,
        });

        const rate: number = 0.6;

        this.renderer.setSize(window.innerWidth * rate, window.innerHeight * rate);
        this.renderer.shadowMap.enabled = true;
        const element: HTMLElement = document.getElementById(this.renderAreaName)!;
        while (element.firstChild) {
            element.removeChild(element.firstChild);
        }

        element.appendChild(this.renderer.domElement);

        const tick = (): void => {

            this.step = (this.step > 1000)? 0 : this.step + 0.05;
            
            light.position.x = 4 * Math.cos(this.step);
            light.position.z = 4 * Math.sin(this.step);
            // light.position.z = 4 * Math.cos(this.step);
            light.lookAt(this.target.position);

            this.renderingID = requestAnimationFrame(tick);
            this.camera.lookAt(this.target.position);
            this.renderer!.render(this.scene, this.camera);
        };

        // レンダリング
        tick();
        // this.render();
    }

    public materialChage(name: string): void {
        // let material = this.materialTable[name]();
    
        this.box.material = this.materialTable[name]();
        this.box.castShadow = true;
    }

    public renderStop(): void {
        cancelAnimationFrame(this.renderingID);
    }

    public get materialNames(): string[] {

        const names: string[] = new Array<string>();

        for (const key in this.materialTable) {
            if (this.materialTable.hasOwnProperty(key)) {
                names.push(key);
            }
        }

        return names;
    }

}
