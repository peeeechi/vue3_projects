import Vue from 'vue';
// import App from '../App.vue';
import Main from '../views/Mainpage.vue';

Vue.config.productionTip = false;

new Vue({
  // router,
  render: (h) => h(Main),
}).$mount('#app');

// new Vue ({
//   el: '#app',
//   components: {
//     Main,
//   },
//   template: `<main />`,
// });


// Vue.extend({
//   components: {
//     Main,
//   },
//   template: `<main></main>`,
// });