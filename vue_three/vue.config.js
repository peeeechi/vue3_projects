module.exports = {
    publicPath: ".",
    outputDir: process.env.NODE_ENV === 'production' ? 'output' : 'dist',
    assetsDir: process.env.NODE_ENV === 'production' ? 'static' : '',
    pages: {
        page1: {
            entry: './src/pages/page1.ts', // エントリーポイントとなるjs, ts
            template: './public/page1.html', // テンプレートのHTML
            filename: 'page1.html' // build時に出力されるファイル名
        },
        page2: {
            entry: './src/pages/page2.ts', // エントリーポイントとなるjs, ts
            template: './public/page2.html', // テンプレートのHTML
            filename: 'page2.html' // build時に出力されるファイル名
        },
    }
}