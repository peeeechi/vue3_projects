import * as express from 'express';
import * as fs from 'fs';
import IImageFile from '../models/IImageFile';
import * as path from 'path';

const fsAsync = fs.promises;

const router = express.Router();


router.post('/', (req: express.Request, res: express.Response, next: express.NextFunction) => {


    res.setHeader('Content-Type', 'application/json');
    const data              = req.body as IImageFile;

    try {
        const buffer            = Buffer.from(data.data, 'base64');
        const typeR             = /(?<type>\S+)\/(?<ext>\S+)/;
        console.log(data.type);
        const match             = data.type.match(typeR);
        const baseDir           = fs.readFileSync(path.join(__dirname, '../../saveDir.txt'));
        console.log(match);
        // const saveFileDir       = (match)? path.join(__dirname, `../../${match.groups['type']}`) : path.join(__dirname, '../../other/');
        const saveFileDir       = (match)? path.join(baseDir.toString(), `${match.groups['type']}`) : path.join(baseDir.toString(), 'other/');
        console.log('save dir: ', saveFileDir);
    
        if (!fs.existsSync(saveFileDir)) {
            console.log('make dir');
            
            fs.mkdirSync(saveFileDir);
        }
    
        var saveFileName: string = path.join(saveFileDir, data.filename);
    
        fsAsync.writeFile(saveFileName, buffer)
        .then(() => {
            const result = {
                status: 'OK',
                saveFile: req.body.filename
            };
            return res.send(JSON.stringify(result));
        })
        .catch((err) => {
            return res.send(JSON.stringify({err: err}));
        })
    } catch (error) {
        console.error(error);
    }



    // fs.writeFile(saveFileName, buffer, (err) => {

    //     if (err) {
    //         return res.send(JSON.stringify({err: err}));
    //     }
    //     else {
    //         const result = {
    //             status: 'OK',
    //             saveFile: req.body.filename
    //         };
    //         return res.send(JSON.stringify(result));
    //     }
    // })


})

export default router;