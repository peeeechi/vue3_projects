// const express = require('express');
import * as express from 'express';
import saveAPI from './route/save';
import * as bodyParser from 'body-parser';
import * as Path from 'path';


const app = express();
const port = 3000;

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json({limit: '900mb'}));

// app.use(Path.join(__dirname, '../../dist/static'), express.static('public'));
app.use('/api/save', saveAPI);
app.use(express.static(Path.join(__dirname, '../../dist')));
// app.set('views', Path.join(__dirname, '../../dist'));

// app.get('/', (req, res) => res.send('Hello World'));
app.get('/', (req, res) => res.sendFile(Path.join(__dirname, '../../dist/index.html')));
app.listen(port, () => console.log(`Example app listening on port ${port}!`));


// catch 404 and forward to error handler
// app.use((req, res, next) => {
//     next(createError.default(404));
// });

// error handler
app.use((err, req, res, next) => {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    // render the error page
    res.status(err.status || 500);

    const data = {
        message: err.message,
        error: err
    }
    res.send(JSON.stringify(data));
});
