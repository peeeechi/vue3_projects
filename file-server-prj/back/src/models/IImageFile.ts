export default interface IImageFile {
    /**
     * ファイル名 (xxx.png 等)
     */
    filename: string|null;

    /**
     * MIME Type (image/png 等)
     */
    type: string|null;

    /**
     * データ base64形式
     */
    data: string|null;
}