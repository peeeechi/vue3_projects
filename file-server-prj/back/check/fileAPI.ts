import * as fs from 'fs';

fs.readdir('./', (err, files) => {

    if (err) {
        console.error(err);
    }

    for (let i = 0; i < files.length; i++) {
        const element = files[i];
        console.log(element);
    }
});

fs.readFile('./nodemon.json',{encoding: 'utf-8'}, (err, data) => {
    if (err) {
        console.error(err);
    }
    else {
        console.log('data: ', data);
    }
    
});


fs.writeFile('./test.txt', 'this is test data.', (err) => {
    if (err) console.error(err);
})