const r = /(?<type>\S+)\/(?<ext>\S+)/

const match = 'image/png'.match(r);

console.log('type: ', match.groups['type']);
console.log('ext: ', match.groups['ext']);
