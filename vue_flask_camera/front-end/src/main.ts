import Vue from 'vue';
import App from './App.vue';
import router from './router';
import store from './store';
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';

Vue.config.productionTip = false;

// import locale from 'element-ui/lib/locale/lang/ja'

new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount('#app');

Vue.use(ElementUI);
