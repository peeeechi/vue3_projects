from flask import Flask, render_template, jsonify, request, Response
import json
from random import *
import cv2, base64
import numpy as np
from backend.path_creater.functions import make2dPath_One, MakeGearPath
from backend.path_creater.param import InnerGearParamA


class Capture(object):
    def __init__(self):
        self.cap = None
        self.isRunning = False
        self.response = None

    def Capture(self):
        if self.cap is None or self.isRunning == False:
            self.cap = cv2.VideoCapture(0)
            self.cap.set(5,60)
            self.isRunning = True
        ret, img = self.cap.read()
        # img = cv2.flip(img,0)
        
        retval, buffer = cv2.imencode('.jpg', img)
        jpg_as_text = base64.b64encode(buffer).decode('utf-8')
        # jpg_as_text = base64.b64encode(buffer)
        # self.cap.release()
        self.response = 'data:image/jpeg;base64,' + jpg_as_text
        data = {"value":self.response}
        # response = make_response(json.dumps(data))
        # response.headers['Content-Type'] = 'application/json'

        return jsonify(data)

    def Release(self):
        if self.isRunning == True:
            self.cap.release()
            self.isRunning = False

        res = {
            "value" : "cap relesed"
        }

        return jsonify(res)

class InvalidUsage(Exception):
    status_code = 400

    def __init__(self, message, status_code=None, payload=None):
        Exception.__init__(self)
        self.message = message
        if status_code is not None:
            self.status_code = status_code
        self.payload = payload

    def to_dict(self):
        rv = dict(self.payload or ())
        rv['message'] = self.message
        return rv


app = Flask(__name__,
            # static_folder   = './dist',   # static folder
            # template_folder = './dist'    # template folder (render_template で使用する)
            static_folder   = './dist/static',   # static folder
            template_folder = './dist'    # template folder (render_template で使用する)
            )

cam = Capture()

# @app.route('/')
# def index():
#     return render_template("index.html")

@app.route('/', defaults={'path': ''})
@app.route('/<path:path>')
def catch_all(path):
    return render_template("index.html")

@app.errorhandler(InvalidUsage)
def handle_invalid_usage(error):
    response = jsonify(error.to_dict())
    response.status_code = error.status_code
    return response

@app.route('/api/random')
def get_random_number():
    res = {
        'value': randint(1,100)
    }

    return jsonify(res)

@app.route('/api/capture')
def capture():
    return cam.Capture()

@app.route('/api/release')
def release():
    return cam.Release()

@app.route('/api/makepath', methods=['POST'])
def makeOnePath():

    data = request.data.decode('utf-8')
    data = json.loads(data)
    print(data)

    try:
        gParam = InnerGearParamA(
            module=float(data["module"]),
            toothNum=int(data["toothNum"]),
            pressAngle=np.radians(float(data["pressAngle"])),
            dislocation=float(data["dislocation"]),
            filletRad=float(data["filletRad"]),
            toothLength=float(data["toothLength"]),
            slopeAngle=np.radians(float(data["slopeAngle"])),
            cChamfer=float(data["cChamfer"])
            )
        
        print(gParam)

        pathDataOne = make2dPath_One(gParam, 0.1)
        pathData = MakeGearPath(gParam, 0.1)

    
        
        res = {
            "onedata": {
                "x": pathDataOne[0].tolist(),
                "y": pathDataOne[1].tolist()
            },
            "fulldata": {
                "x": pathData[0].tolist(),
                "y": pathData[1].tolist()
            }
        }
    except:
        print("error")
        res = {
            "data": {
                "x": [],
                "y": []
            }
        }

    print(res)
    return jsonify(res)
        # raise InvalidUsage('arg error', status_code=410)

if __name__ == "__main__":   
    
    app.run(
        host='0.0.0.0',
        # host='127.0.0.1',
        port=5500,
        debug=True 
        )
